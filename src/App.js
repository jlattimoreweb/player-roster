import React, {useState} from 'react'

import Players from './Components/Players'
import { createMuiTheme } from '@material-ui/core/styles';
import {FilterContext} from './Components/context/filterContext'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#757ce8',
      main: '#3f50b5',
      dark: '#002884',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#000',
    },
  },
});

function App() {
  const [filterVal, setFilterVal] = useState({status: "", region: ""})

  return (
    <div className="App" title="App">
      <FilterContext.Provider value={{filterVal, setFilterVal}}>
        <Players theme={theme}/>
      </FilterContext.Provider>
    </div>
  )
}

export default App;
