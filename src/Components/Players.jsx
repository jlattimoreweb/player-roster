import React, {useState, useContext, useEffect} from 'react'
import PlayerInfo from './PlayerInfo'
import Filters from './Filters'
import { Grid, Typography } from '@material-ui/core'
import { withTheme } from '@material-ui/core/styles'
import axios from 'axios'
import {FilterContext} from './context/filterContext'

const Players = () => {
    const [list, setList] = useState([])
    const {filterVal} = useContext(FilterContext)

    useEffect(() => {
      axios.get(process.env.REACT_APP_API_URL)
      .then((results) => setList(results.data))
    })

    return (
      <div>
        <Typography variant="h3" color="primary">2021 Player Directory</Typography>
          <Filters />
          <Grid container>
            {filterVal.status !== "" && filterVal.region !== "" && list.filter((entry) => entry.status == filterVal.status).filter((entry) => entry.state == filterVal.region).map((player, i) =>
                // fill out below props from results of API
                <PlayerInfo key={i}
                  name={player.name}
                  age={player.age}
                  state={player.state}
                  gender={player.gender}
                  status={player.status}
                />
            )}
            {filterVal.status !== "" && filterVal.region === "" && list.filter((entry) => entry.status == filterVal.status).map((player, i) =>
                // fill out below props from results of API
                <PlayerInfo key={i}
                  name={player.name}
                  age={player.age}
                  state={player.state}
                  gender={player.gender}
                  status={player.status}
                />
            )}
            {filterVal.status === "" && filterVal.region !== "" && list.filter((entry) => entry.state == filterVal.region).map((player, i) =>
                // fill out below props from results of API
                <PlayerInfo key={i}
                  name={player.name}
                  age={player.age}
                  state={player.state}
                  gender={player.gender}
                  status={player.status}
                />
            )}
            {filterVal.status === "" && filterVal.region === "" && list.map((player, i) =>
                // fill out below props from results of API
                <PlayerInfo key={i}
                  name={player.name}
                  age={player.age}
                  state={player.state}
                  gender={player.gender}
                  status={player.status}
                />
            )}
          </Grid>
    </div>
  )
}

export default withTheme(Players)
