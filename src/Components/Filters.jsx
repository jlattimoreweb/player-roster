import React, {Fragment, useContext} from 'react'
import { Button, Grid, MenuItem, Select, Typography } from '@material-ui/core'
import { states } from '../data/states'
import { withTheme } from '@material-ui/core/styles'
import {FilterContext} from './context/filterContext'

const Filters = () => {
  const {filterVal, setFilterVal} = useContext(FilterContext)
  return (
    <Fragment>
      <Grid container>
        <Grid item xs={2}><Typography variant="h5" color="secondary">Filter By:</Typography></Grid>
        <Grid item xs={5}>
          <Typography variant="subtitle2">Status</Typography>
          <Button variant="outlined" color="primary" id="activeButton" onClick={() => setFilterVal({status: 'active', region: filterVal.region})}>Active</Button>
          <Button variant="outlined" color="secondary" id="inactiveButton" onClick={() => setFilterVal({status: 'inactive', region: filterVal.region})}>Inactive</Button>
        </Grid>
        <Grid item xs={3}>
          <Typography variant="subtitle2">State</Typography>
          <Select id="stateFilter" value={""}>
            {states.map((region, i) =>
              <MenuItem key={i} value={region.name} onClick={() => setFilterVal({status: filterVal.status, region: region.abbreviation})}>{region.name}</MenuItem>
            )}
          </Select>
        </Grid>
        <Grid item xs={2}>
          <Typography variant="subtitle2">Reset</Typography>
          <Button variant="outlined" color="primary" id="clearButton" onClick={() => setFilterVal({status: "", region: ""})}>Clear</Button>
        </Grid>
      </Grid>
    </Fragment>
  )
}

export default withTheme(Filters)
